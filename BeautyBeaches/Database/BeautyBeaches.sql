-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 26, 2022 lúc 11:19 PM
-- Phiên bản máy phục vụ: 10.4.22-MariaDB
-- Phiên bản PHP: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `beautybeaches`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bbadmin`
--
create database beautybeaches;
use beautybeaches;

CREATE TABLE `bbadmin` (
  `USERNAME` varchar(100) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `bbadmin`
--

INSERT INTO `bbadmin` (`USERNAME`, `PASSWORD`) VALUES
('admin', '7c4a8d09ca3762af61e59520943dc26494f8941b'),
('admin2', '7c4a8d09ca3762af61e59520943dc26494f8941b'),
('admin3', '7c4a8d09ca3762af61e59520943dc26494f8941b'),
('admin4', '7c4a8d09ca3762af61e59520943dc26494f8941b'),
('admin5', '42cfe854913594fe572cb9712a188e829830291f');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bbbooking`
--

CREATE TABLE `bbbooking` (
  `BookingID` int(20) NOT NULL,
  `FirstName` varchar(40) DEFAULT NULL,
  `LastName` varchar(40) DEFAULT NULL,
  `DateOfVisit` datetime DEFAULT NULL,
  `Email` varchar(40) NOT NULL,
  `NumberOfPerson` varchar(40) DEFAULT NULL,
  `Location` varchar(40) NOT NULL,
  `Note` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `bbbooking`
--

INSERT INTO `bbbooking` (`BookingID`, `FirstName`, `LastName`, `DateOfVisit`, `Email`, `NumberOfPerson`, `Location`, `Note`) VALUES
(1, 'tran', 'hoang', '2022-07-26 00:00:00', 'hoangbk99@gmail.com', '1', 'France', 'abcdxyz'),
(2, 'duong', 'hai', '2022-07-26 00:00:00', 'haiduong@gmail.com', '1', 'Hawaii', 'abcdxyz');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `blog`
--

CREATE TABLE `blog` (
  `blogName` varchar(255) NOT NULL,
  `blogImage` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `blog`
--

INSERT INTO `blog` (`blogName`, `blogImage`, `description`, `content`) VALUES
('Ha Long Bay\r\n', 'https://statics.vinpearl.com/ha-long-bay-in-vietnam-2_1648434936.jpg', 'by Tran Viet Hoang\r\n', 'Hạ Long Bay or Halong Bay (Vietnamese: Vịnh Hạ Long is a UNESCO World Heritage Site and popular travel destination in Quảng Ninh Province, Vietnam. The name Hạ Long means \"descending dragon\". Administratively, the bay belongs to Hạ Long city, Cẩm Phả city, and is a part of Vân Đồn District. The bay features thousands of limestone karsts and isles in various shapes and sizes. Ha Long Bay is a center of a larger zone which includes Bai Tu Long Bay to the northeast, and Cát Bà Island to the southwest. These larger zones share a similar geological, geographical, geomorphological, climate, and cultural characters.\n\n'),
('How to get from El Nido to Coron: With a Short El Nido Guide\r\n', 'https://res.klook.com/image/upload/c_fill,w_750,h_500,f_auto/w_80,x_15,y_15,g_south_west,l_Klook_water_br_trans_yhcmh3/activities/vlqudn1zo26jw6em1uwm.jpg', 'by Tran Viet Hoang\r\n', 'If you are planning to get from El Nido to Coron, then in this article I will mention the perfect ways how to reach this paradise. It will surely reduce a lot of stress and hustle around organizing your transfer. We don’t have to introduce El Nido to beach lovers. El Nido belongs to one of the most beautiful places in the Philippines and there are many reasons why. Known for its turquoise-blue lagoons, amazing scenery and some of the best beaches in Southeast Asia, this part of the world belongs to heaven. After visiting beautiful Bohol island and Cebu island, Palawan was our last stop in the Philippines.'),
('Plage de la Coote des Basques, Biarritz, Aquitaine\r\n', 'https://www.lonelyplanet.fr/sites/lonelyplanet/files/styles/article_main_image/public/media/article/image/adobestock_119084545.jpeg?itok=OjIQSx-g', 'by Nguyen Hai Duong', 'This is a famous resort paradise for royalty and famous artists. Dubbed “wild beaches”, the Plage de la Côte des Basques in Biarritz, Basque Country, located along the Bay of Biscay is also a popular surfing spot. The waves here are very safe for beginners to learn to surf.'),
('The best things to see in Broome, Western Australia\r\n', 'https://www.beachaddicted.com/wordpresss-beachaddicted/wp-content/uploads/2021/07/Broome_0190-1.jpg', 'by Vu Ngoc Long \r\n', 'If you are looking for the best things to see in Broome, then you came to the right place. We really liked Broome. After driving through the Australian outback and Northern territory we were really looking forward to hopping into the water. Luckily Broome has many beautiful beaches, stunning sunsets, and happy camels running around the beach. Broome also has the biggest tides in the Southern Hemisphere with a difference of up to 10 meters between high and low tide.');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `location`
--

CREATE TABLE `location` (
  `locationID` int(20) NOT NULL,
  `location` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `location`
--

INSERT INTO `location` (`locationID`, `location`) VALUES
(6, 'France'),
(4, 'Hawaii'),
(5, 'India'),
(3, 'Japan'),
(1, 'Philipines'),
(2, 'Vietnam');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `note`
--

CREATE TABLE `note` (
  `noteID` int(20) NOT NULL,
  `FirstName` varchar(40) NOT NULL,
  `LastName` varchar(40) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `Subject` varchar(255) NOT NULL,
  `Message` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `note`
--

INSERT INTO `note` (`noteID`, `FirstName`, `LastName`, `Email`, `Subject`, `Message`) VALUES
(1, 'tran', 'hoang', 'hoangbk989@gmail.com', 'abcdxyz', 'abxczxczx'),
(2, 'tran', 'hoang', 'hoangbk989@gmail.com', 'tôi thắc mắc abcdxyz', 'tại sao lại thế này thế kia');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `Product_ID` int(20) NOT NULL,
  `Location` varchar(40) NOT NULL,
  `LocationImage` varchar(255) DEFAULT NULL,
  `Price` varchar(255) DEFAULT NULL,
  `Number` int(100) DEFAULT NULL,
  `Note` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`Product_ID`, `Location`, `LocationImage`, `Price`, `Number`, `Note`) VALUES
(1, 'Philippines', 'https://i.natgeofe.com/n/04505c35-858b-4e95-a1a7-d72e5418b7fc/steep-karst-cliffs-of-el-nido-in-palawan.jpg?w=2880&amp;h=1440', '2.000.000 đ', 2, ' Philippines nằm cách đảo Đài Loan qua eo biển Luzon ở phía Bắc, cách Việt Nam qua biển Đông ở phía Tây, cách đảo Borneo của Indonesia qua biển Sulu ở phía Tây Nam và các đảo khác của nước này qua biển Celebes ở phía Nam, phía đông là biển Philippines và đảo quốc Palau. Philippines nằm trên Vành đai núi lửa Thái Bình Dương và gần với đường xích đạo, do vậy, quốc gia này hằng năm thường xuyên bị ảnh hưởng bởi các trận động đất và bão nhiệt đới - đây là quốc gia phải hứng chịu nhiều thiên tai vào bậc nhất trên toàn cầu'),
(2, 'Vietnam', 'https://i.ytimg.com/vi/lTCsqTlcjvg/maxresdefault.jpg', '2.000.000 đ', 1, 'Việt Nam, quốc hiệu chính thức là Cộng hòa Xã hội chủ nghĩa Việt Nam, là một quốc gia nằm ở cực Đông của bán đảo Đông Dương thuộc khu vực Đông Nam Á, giáp với Lào, Campuchia, Trung Quốc, biển Đông và vịnh Thái Lan.'),
(3, 'Japan', 'https://www.worldtravelguide.net/wp-content/uploads/2018/08/Oth-Japan-Takahama-SUP-1440x823.jpg', '10.000.000 đ', 1, 'Nhật Bản là quốc gia đông thứ 11 thế giới, đồng thời là một trong những quốc gia có mật độ dân số và đô thị hóa cao nhất. Khoảng 3/4 địa hình của Nhật Bản là đồi núi, tập trung dân số 125,44 triệu người trên các đồng bằng nhỏ hẹp ven biển. Quốc gia này được chia thành 47 tỉnh thuộc 8 vùng địa lý. Khu vực thủ đô Tokyo là đại đô thị đông dân nhất thế giới với 37,4 triệu dân.'),
(4, 'Hawaii', 'https://www.gousa.in/sites/default/files/styles/state_hero_l/public/images/hero_media_image/2018-10/d81ad4c31764249b09e3a57f9ca04403.jpeg?itok=peKzFoRL', '30.000.000 đ', 1, 'Hawaiʻi là tiểu bang duy nhất của Hoa Kỳ có nước xung quanh. Vì không thuộc lục địa Hoa Kỳ, nó là một trong hai tiểu bang không giáp với tiểu bang khác (Alaska là tiểu bang kia). Nó cũng là cực nam của Hoa Kỳ, là tiểu bang duy nhất nằm hoàn toàn trong miền nhiệt đới, và tiểu bang duy nhất không thuộc về châu lục nào. Hawaiʻi cũng là tiểu bang duy nhất đang tiếp tục nâng lên, do các dòng dung nham đang chảy, nhất là từ núi lửa Kīlauea.'),
(5, 'India', 'http://cdn.cnn.com/cnnnext/dam/assets/180127075651-india-best-beaches-tarkarli-maharashtra.jpg', '9.000.000 đ', 3, 'Tiểu lục địa Ấn Độ là nơi khởi nguồn của nền văn minh lưu vực sông Ấn cổ đại, sớm hình thành nên các tuyến đường mậu dịch mang tính quốc tế cùng những Đế quốc rộng lớn, các Đế quốc này trở nên giàu có, thịnh vượng do thương mại cùng sức mạnh văn hóa - quân sự mang lại trong suốt chiều dài lịch sử của mình.[23] Đây cũng là nơi khởi nguồn của 4 tôn giáo lớn trên thế giới bao gồm: Ấn Độ giáo, Phật giáo, Jaina giáo và Sikh giáo;'),
(6, 'France', 'https://cdn.theculturetrip.com/wp-content/uploads/2017/01/villefranche-sur-mer-1265940_1280.jpg', '10.000.000 đ', 5, 'Nếu tính luôn các lãnh thổ khác ngoài châu Nam cực thì khoảng 674.000  km². Pháp là nước cộng hòa bán tổng thống nhất thể, thủ đô Paris cũng là thành phố lớn nhất, trung tâm văn hóa và thương mại chính của quốc gia. Các trung tâm đô thị lớn khác là Marseille, Lyon, Lille, Nice, Toulouse và Bordeaux.');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bbadmin`
--
ALTER TABLE `bbadmin`
  ADD UNIQUE KEY `USERNAME` (`USERNAME`);

--
-- Chỉ mục cho bảng `bbbooking`
--
ALTER TABLE `bbbooking`
  ADD PRIMARY KEY (`BookingID`),
  ADD KEY `Location` (`Location`);

--
-- Chỉ mục cho bảng `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`blogName`),
  ADD KEY `content` (`content`(768));

--
-- Chỉ mục cho bảng `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`locationID`),
  ADD KEY `location` (`location`);

--
-- Chỉ mục cho bảng `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`noteID`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`Product_ID`),
  ADD KEY `Location` (`Location`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bbbooking`
--
ALTER TABLE `bbbooking`
  MODIFY `BookingID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `location`
--
ALTER TABLE `location`
  MODIFY `locationID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `note`
--
ALTER TABLE `note`
  MODIFY `noteID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `Product_ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `bbbooking`
--
ALTER TABLE `bbbooking`
  ADD CONSTRAINT `bbbooking_ibfk_1` FOREIGN KEY (`BookingID`) REFERENCES `products` (`Product_ID`);

--
-- Các ràng buộc cho bảng `products`
--

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
