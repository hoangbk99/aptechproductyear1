<?php 
    require_once "connect.php";
    //  if(!$conn_createdb){
    //         die("Kết nối thất bại".mysqli_connect_error());   
    //     }else{
    //         echo"Kết nối thành công";
    //     };
?>

<?php
    if(isset($_POST['danhsach'])){
        header("location: products.php");
    }
?>

<?php

$error_name ='';
$error_img ='';
$error_gia ='';
$error_soluong ='';

if(isset($_POST['oke'])){
    if ($_POST['Location'] == '' || empty($_POST['Location'])) {
        $error_name =  '<div style="color:red;margin-left:205px;wProduct_IDth:260px">Tên không được để trống</div>';
}
    if ($_POST['LocationImage'] == '' || empty($_POST['LocationImage'])) {
        $error_img =  '<div style="color:red;margin-left:205px;wProduct_IDth:260px">Ảnh không được để trống</div>';
}
    if ($_POST['Price'] == '' || empty($_POST['Price'])) {
        $error_gia =  '<div style="color:red;margin-left:205px;wProduct_IDth:260px">Giá không được để trống</div>';
}
    if ($_POST['Number'] == '' || empty($_POST['Number'])) {
        $error_soluong =  '<div style="color:red;margin-left:205px;wProduct_IDth:260px">Số lượng không được để trống</div>';
}
}
?>

<?php
$thanhcong='';
$thatbai='';
$nhaplai = ''; 
if(isset($_POST['oke'])){
    $Product_ID = $_POST['Product_ID'];
    $Location = $_POST['Location'];
    $LocationImage = $_POST['LocationImage'];
    $Price = $_POST['Price'];
    $Number = $_POST['Number'];
    $Note = $_POST['Note'];

    if($Location != '' && $LocationImage != '' && $Price != '' && $Number != '' && $Note != ''){
    $sql=sprintf("insert into products values('%s','%s','%s','%s','%s','%s')", $Product_ID,$Location,$LocationImage,$Price,$Number,$Note);
    //var_dump($query);

    if($conn_createdb->query($sql)){
        $thanhcong = '<div style="color:red;margin-left:205px;margin-top:10px">Thêm thành công</div>';
    }else{
        $thatbai = '<div style="color:red;margin-left:205px;margin-top:10px">Thêm thất bại</div>';
    }
    }else{
        $nhaplai = '<div style="color:red;margin-left:205px;margin-top:10px">Yêu cầu nhập thông tin</div>';
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        label{margin: 5px 10px;width: 12%;display: inline-block;}
        input{margin:0px 5px;}
    </style>
</head>
<body>
    
        <div style="height:100px;background:#ECF0FE;">
        <div style="margin-left:100px;line-height:100px;font-size:30px">Thêm Địa Điểm</div>
        </div>
        <hr>
        <form method="post" action="">
        <label>Product_ID</label><input type="number" name="Product_ID">
        <br>
        <label>Tên Địa Điểm: </label><input type="text" name="Location" >
        <?php echo $error_name ?>
        <br>
        <label>URL ảnh địa điểm: </label><input type="text" name="LocationImage" >
        <?php echo $error_img ?>
        <br>
        <label>Giá: </label><input type="text" name="Price" >
        <?php echo $error_gia ?>
        <br>
        <label>Số Lượng: </label><input type="text" name="Number" >
        <?php echo $error_soluong ?>
        <br>
        <label >Ghi chú: </label><input style="height:200px" type="text" name="Note" >
        <?php echo $thanhcong ?>
        <?php echo $thatbai ?>
        <?php echo $nhaplai ?>
        <br>

        <label>&nbsp;</label> <input type="submit" value="Thêm Địa Điểm" name="oke" >
        <br>
        <label>&nbsp;</label> <input type="submit" value="Danh sách" name="danhsach">
        <br>
        <a href="products.php">Quay lại trang admin</a>
        </form>
    
</body>
</html>