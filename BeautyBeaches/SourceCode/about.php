<!DOCTYPE html>
<html lang="en">
  <head>
    <title>BeautyBeaches &mdash;All you can go!</title>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700"
    />
    <link rel="stylesheet" href="fonts/icomoon/style.css" />

    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link rel="stylesheet" href="css/owl.carousel.min.css" />
    <link rel="stylesheet" href="css/owl.theme.default.min.css" />

    <link rel="stylesheet" href="css/bootstrap-datepicker.css" />

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css" />

    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css"
    />

    <link rel="stylesheet" href="css/aos.css" />

    <link rel="stylesheet" href="css/style.css" />
  </head>
  <body>
    <div class="site-wrap">
      <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>

      <header class="site-navbar py-1" role="banner">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-6 col-xl-2">
              <h1 class="mb-0">
                <a href="index.php" class="text-black h2 mb-0">BeautyBeaches</a>
              </h1>
            </div>
            <div class="col-10 col-md-8 d-none d-xl-block">
              <nav
                class="site-navigation position-relative text-right text-lg-center"
                role="navigation"
              >
                <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                  <li class="active">
                    <a href="index.php">Home</a>
                  </li>
                  <li>
                    <a href="destination.php">Location</a>
                    <!-- <ul class="dropdown">
                      <li class="has-children">
                        Asia
                        <ul>
                          <li class="theli">
                            <a href="blog.php"
                              ><span class="thea">Việt Nam</span></a
                            >
                          </li>
                          <li class="theli">
                            <a href="blog.php"
                              ><span class="thea">Indonesia</span></a
                            >
                          </li>
                        </ul>
                      </li>

                      <li>
                        Americas
                        <ul>
                          <li class="theli">
                            <a href="blog.php"><span class="thea">U.S</span></a>
                          </li>
                          <li class="theli">
                            <a href="blog.php"
                              ><span class="thea">Mexico</span></a
                            >
                          </li>
                        </ul>
                      </li>
                      <li>
                        Australia
                        <ul>
                          <li class="theli">
                            <a href="blog.php"
                              ><span class="thea">Australia</span></a
                            >
                          </li>
                          <li class="theli">
                            <a href="blog.php"
                              ><span class="thea">Australia</span></a
                            >
                          </li>
                        </ul>
                      </li>

                      <li>
                        Europe
                        <ul>
                          <li class="theli">
                            <a href="blog.php"
                              ><span class="thea">France</span></a
                            >
                          </li>
                          <li class="theli">
                            <a href="blog.php"
                              ><span class="thea">Bulgaria</span></a
                            >
                          </li>
                        </ul>
                      </li>
                    </ul> -->
                  </li>

                  <li><a href="discount.php">Discount</a></li>
                  <li><a href="booking.php">Book</a></li>
                  <li><a href="about.php">About</a></li>
                  <li >
                    <a href="blog.php">Blog</a>
                      <li>
                        <a href="blog.php"></a>
                      </li>
                  </li>

                  <li><a href="contact.php">Contact</a></li>
                </ul>
              </nav>
            </div>

            <div class="col-6 col-xl-2 text-right">
              <div class="d-none d-xl-inline-block">
                <ul
                  class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0"
                  data-class="social"
                >
                  <li>
                    <a
                      href="https://www.tripadvisor.com.vn/"
                      target="_blank"
                      class="pl-0 pr-3 text-black"
                      ><span class="icon-tripadvisor"></span
                    ></a>
                  </li>
                  <li>
                    <a
                      href="https://twitter.com/?lang=vi"
                      target="_blank"
                      class="pl-3 pr-3 text-black"
                      ><span class="icon-twitter"></span
                    ></a>
                  </li>
                  <li>
                    <a
                      href="https://www.facebook.com/"
                      target="_blank"
                      class="pl-3 pr-3 text-black"
                      ><span class="icon-facebook"></span
                    ></a>
                  </li>
                  <li>
                    <a
                      href="https://www.instagram.com/"
                      target="_blank"
                      class="pl-3 pr-3 text-black"
                      ><span class="icon-instagram"></span
                    ></a>
                  </li>
                  <li>
                    <a href="adminlogin.php" class="pl-3 pr-3 text-black"
                      ><span>Admin</span></a
                    >
                  </li>
                </ul>
              </div>

              <div
                class="d-inline-block d-xl-none ml-md-0 mr-auto py-3"
                style="position: relative; top: 3px"
              >
                <a href="index.php" class="site-menu-toggle js-menu-toggle text-black"
                  ><span class="icon-menu h3"></span
                ></a>
              </div>
            </div>
          </div>
        </div>
      </header>

      <div
        class="site-blocks-cover inner-page-cover"
        style="background-image: url(images/img_4.jpg)"
        data-aos="fade"
        data-stellar-background-ratio="0.5"
      >
        <div class="container">
          <div
            class="row align-items-center justify-content-center text-center"
          >
            <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
              <h1 class="text-white font-weight-light">About BeautyBeaches</h1>
              <div>
                <a href="index.php">Home</a>
                <span class="mx-2 text-white">&bullet;</span>
                <span class="text-white">About</span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="site-section" data-aos="fade-up">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-md-6 mb-5 mb-md-0">
              <img
                src="images/aptech.jpg"
                alt="Image"
                class="img-fluid rounded"
              />
            </div>
            <div class="col-md-6 pl-md-5">
              <h2 class="font-weight-light text-black mb-4">About Company</h2>
              <p>
                Nurtured from the seed of a single great idea - to empower the
                traveller - BeautyBeaches is a pioneer in Vietnam's online
                travel industry. Founded in the year 2000 by TranVietHoang,
                BeautyBeaches came to life to empower the Vietnamese traveller
                with instant bookings and comprehensive choices.
              </p>

              <ul class="list-unstyled">
                <li class="d-flex align-items-center">
                  <span class="icon-check2 text-primary h3 mr-2"></span
                  ><span> Life's sort of unencumbered</span>
                </li>
                <li class="d-flex align-items-center">
                  <span class="icon-check2 text-primary h3 mr-2"></span
                  ><span>All-at-most Prosecutors</span>
                </li>
                <li class="d-flex align-items-center">
                  <span class="icon-check2 text-primary h3 mr-2"></span
                  ><span>The distinction of times</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="site-section">
        <div class="container">
          <div class="row justify-content-center mb-5" data-aos="fade-up">
            <div class="col-md-7">
              <h2 class="font-weight-light text-black text-center">Our Team</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-lg-4 text-center mb-5" data-aos="fade-up">
              <img
                src="images/hoang.jpg"
                alt="Image"
                class="img-fluid w-50 rounded-circle mb-4"
              />
              <h2 class="text-black font-weight-light mb-4">Tran Viet Hoang</h2>
              <p class="mb-4">
                Pain itself is a lot of pain Is it the result of people who are
                not easily blinded by it, hating it, repelling discrimination,
                because it will happen something that makes the wise docile not
                appeased and bound to continue?
              </p>
              <p>
                <a href="about.php" class="pl-0 pr-3"
                  ><span class="icon-twitter"></span
                ></a>
                <a href="about.php" class="pl-3 pr-3"
                  ><span class="icon-instagram"></span
                ></a>
                <a
                  href="https://www.facebook.com/hoang.tran.90663894"
                  class="pl-3 pr-3"
                  ><span class="icon-facebook"></span
                ></a>
              </p>
            </div>
            <div class="col-md-6 col-lg-4 text-center mb-5" data-aos="fade-up">
              <img
                src="images/duong.jpg"
                alt="Image"
                class="img-fluid w-50 rounded-circle mb-4"
              />
              <h2 class="text-black font-weight-light mb-4">
                Nguyen Hai Duong
              </h2>
              <p class="mb-4">
                Pain itself is a lot of pain Is it the result of people who are
                not easily blinded by it, hating it, repelling discrimination,
                because it will happen something that makes the wise docile not
                appeased and bound to continue?
              </p>
              <p>
                <a href="https://twitter.com/trum2506" class="pl-0 pr-3"
                  ><span class="icon-twitter"></span
                ></a>
                <a href="https://www.instagram.com/trum.2506/" class="pl-3 pr-3"
                  ><span class="icon-instagram"></span
                ></a>
                <a href="https://www.facebook.com/Trum.2506" class="pl-3 pr-3"
                  ><span class="icon-facebook"></span
                ></a>
              </p>
            </div>
            <div class="col-md-6 col-lg-4 text-center mb-5" data-aos="fade-up">
              <img
                src="images/long.jpg"
                alt="Image"
                class="img-fluid w-50 rounded-circle mb-4"
              />
              <h2 class="text-black font-weight-light mb-4">Vu Ngoc Long</h2>
              <p class="mb-4">
                Pain itself is a lot of pain Is it the result of people who are
                not easily blinded by it, hating it, repelling discrimination,
                because it will happen something that makes the wise docile not
                appeased and bound to continue?
              </p>
              <p>
                <a href="about.php" class="pl-0 pr-3"
                  ><span class="icon-twitter"></span
                ></a>
                <a href="about.php" class="pl-3 pr-3"
                  ><span class="icon-instagram"></span
                ></a>
                <a
                  href="https://www.facebook.com/LoveLier7803"
                  class="pl-3 pr-3"
                  ><span class="icon-facebook"></span
                ></a>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="site-section block-13 bg-light">
        <div class="container" data-aos="fade">
          <div class="row justify-content-center mb-5">
            <div class="col-md-7">
              <h2 class="font-weight-light text-black text-center">
                What People Says
              </h2>
            </div>
          </div>

          <div class="nonloop-block-13 owl-carousel">
            <div class="item">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 mb-5">
                    <img
                      src="images/hoang.jpg"
                      alt="Image"
                      class="img-md-fluid"
                    />
                  </div>
                  <div
                    class="overlap-left col-lg-6 bg-white p-md-5 align-self-center"
                  >
                    <p class="text-black lead">
                      &ldquo;As a frequent traveller, it is very easy using
                      BeautyBeaches services&rdquo;
                    </p>
                    <p class="">
                      &mdash; <em>Tran Viet Hoang</em>, <a href="blog.php">Traveler</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 mb-5">
                    <img
                      src="images/duong.jpg"
                      alt="Image"
                      class="img-md-fluid"
                    />
                  </div>
                  <div
                    class="overlap-left col-lg-6 bg-white p-md-5 align-self-center"
                  >
                    <p class="text-black lead">
                      &ldquo;As a frequent traveller, it is very easy using
                      BeautyBeaches services&rdquo;
                    </p>
                    <p class="">
                      &mdash; <em>Nguyen Hai Duong</em>,
                      <a href="blog.php">Traveler</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 mb-5">
                    <img
                      src="images/long.jpg"
                      alt="Image"
                      class="img-md-fluid"
                    />
                  </div>
                  <div
                    class="overlap-left col-lg-6 bg-white p-md-5 align-self-center"
                  >
                    <p class="text-black lead">
                      &ldquo;As a frequent traveller, it is very easy using
                      BeautyBeaches services&rdquo;
                    </p>
                    <p class="">
                      &mdash; <em>Vu Ngoc Long</em>, <a href="blog.php">Traveler</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="site-section border-top">
        <div class="container">
          <div class="row text-center">
            <div class="col-md-12">
              <h2 class="mb-5 text-black">Want To Travel With Us?</h2>
              <p class="mb-0">
                <a
                  href="booking.php"
                  class="btn btn-primary py-3 px-5 text-white"
                  >Book Now</a
                >
              </p>
            </div>
          </div>
        </div>
      </div>

      <footer class="site-footer">
        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <div class="mb-5">
                <h3 class="footer-heading mb-4">About BeautyBeaches</h3>
                <p>
                  Nurtured from the seed of a single great idea - to empower the
                  traveller - BeautyBeaches is a pioneer in Vietnam's online
                  travel industry. Founded in the year 2000 by TranVietHoang,
                  BeautyBeaches came to life to empower the Vietnamese traveller
                  with instant bookings and comprehensive choices.
                </p>
              </div>
            </div>
            <div class="col-lg-4 mb-5 mb-lg-0">
              <div class="row mb-5">
                <div class="col-md-12">
                  <h3 class="footer-heading mb-4">Navigations</h3>
                </div>
                <div class="col-md-6 col-lg-6">
                  <ul class="list-unstyled">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="destination.php">Destination</a></li>
                    <li><a href="booking.php">Services</a></li>
                    <li><a href="about.php">About</a></li>
                  </ul>
                </div>
                <div class="col-md-6 col-lg-6">
                  <ul class="list-unstyled">
                    <li><a href="about.php">About Us</a></li>
                    <li><a href="contact.php">Privacy Policy</a></li>
                    <li><a href="contact.php">Contact Us</a></li>
                    <li><a href="discount.php">Discounts</a></li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="col-lg-4 mb-5 mb-lg-0">
              <div class="mb-5">
                <h3 class="footer-heading mb-2">Subscribe Newsletter</h3>
                <p>
                  The pain itself is the love of the main adipisicing elite the
                  least hatred.
                </p>

                <form action="#" method="post">
                  <div class="input-group mb-3">
                    <input
                      type="text"
                      class="form-control border-secondary text-white bg-transparent"
                      placeholder="Enter Email"
                      aria-label="Enter Email"
                      aria-describedby="button-addon2"
                    />
                    <div class="input-group-append">
                      <button
                        class="btn btn-primary text-white"
                        type="button"
                        id="button-addon2"
                      >
                        Send
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="row pt-5 mt-5 text-center">
            <div class="col-md-12">
              <div class="mb-5">
                <a
                  href="https://www.facebook.com/"
                  target="_blank"
                  class="pl-3 pr-3 text-black"
                  ><span class="icon-facebook"></span
                ></a>
                <a
                  href="https://twitter.com/?lang=vi"
                  target="_blank"
                  class="pl-3 pr-3 text-black"
                  ><span class="icon-twitter"></span
                ></a>
                <a
                  href="https://www.instagram.com/"
                  target="_blank"
                  class="pl-3 pr-3 text-black"
                  ><span class="icon-instagram"></span
                ></a>
                <a
                  href="https://www.linkedin.com/"
                  target="_blank"
                  class="pl-3 pr-3"
                  ><span class="icon-linkedin"></span
                ></a>
              </div>

              <p>
                Copyright &copy;
                <script>
                  document.write(new Date().getFullYear());
                </script>
                All rights reserved
              </p>
            </div>
          </div>
        </div>
      </footer>
    </div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/jquery-migrate-3.0.1.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/aos.js"></script>

    <script src="js/main.js"></script>
  </body>
</html>
