<!DOCTYPE html>
<html lang="en">
  <head>
    <title>BeautyBeaches &mdash;All you can go!</title>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700"
    />
    <link rel="stylesheet" href="fonts/icomoon/style.css" />

    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link rel="stylesheet" href="css/owl.carousel.min.css" />
    <link rel="stylesheet" href="css/owl.theme.default.min.css" />

    <link rel="stylesheet" href="css/bootstrap-datepicker.css" />

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css" />

    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css"
    />

    <link rel="stylesheet" href="css/aos.css" />

    <link rel="stylesheet" href="css/style.css" />
  </head>
  <body>
    <div class="site-wrap">
      <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>

      <header class="site-navbar py-1" role="banner">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-6 col-xl-2">
              <h1 class="mb-0">
                <a href="index.php" class="text-black h2 mb-0"
                  >BeautyBeaches</a
                >
              </h1>
            </div>
            <div class="col-10 col-md-8 d-none d-xl-block">
              <nav
                class="site-navigation position-relative text-right text-lg-center"
                role="navigation"
              >
                <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                  <li class="active">
                    <a href="index.php">Home</a>
                  </li>
                  <li  >
                    <a href="destination.php">Location</a>
                  <!-- <?php
                    require_once('connect.php');
                    $sql = sprintf("select * from location order by locationID asc");
                    $result=$conn_createdb->query($sql);
                    echo '<ul class="dropdown">';
                    while($row = $result->fetch_assoc()){
                    echo '<li class="has-children">';
                    echo  $row["link"];
                    echo  $row["location"];
                    echo '</li>';
                  }
                  echo '</ul>';
                  ?>   -->

                  <li><a href="discount.php">Discount</a></li>
                  <li><a href="booking.php">Book</a></li>
                  <li><a href="about.php">About</a></li>
                  <li><a href="blog.php">Blog</a></li>
                  <li><a href="contact.php">Contact</a></li>
                  <!-- <li><a href="userlogin.php">Login</a></li> -->
                </ul>
              </nav>
            </div>

            <div class="col-6 col-xl-2 text-right">
              <div class="d-none d-xl-inline-block">
                <ul
                  class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0"
                  data-class="social"
                >
                  <li>
                    <a
                      href="https://www.tripadvisor.com.vn/"
                      class="pl-0 pr-3 text-black"
                      ><span class="icon-tripadvisor"></span
                    ></a>
                  </li>
                  <li>
                    <a
                      href="https://twitter.com/?lang=vi"
                      class="pl-3 pr-3 text-black"
                      ><span class="icon-twitter"></span
                    ></a>
                  </li>
                  <li>
                    <a
                      href="https://www.facebook.com/"
                      class="pl-3 pr-3 text-black"
                      ><span class="icon-facebook"></span
                    ></a>
                  </li>
                  <li>
                    <a
                      href="https://www.instagram.com/"
                      class="pl-3 pr-3 text-black"
                      ><span class="icon-instagram"></span
                    ></a>
                  </li>
                  <li>
                    <a href="adminlogin.php" class="pl-3 pr-3 text-black"
                      ><span>Admin</span></a
                    >
                  </li>
                </ul>
              </div>
              <div
                class="d-inline-block d-xl-none ml-md-0 mr-auto py-3"
                style="position: relative; top: 3px"
              >
                <a href="index.php" class="site-menu-toggle js-menu-toggle text-black"
                  ><span class="icon-menu h3"></span
                ></a>
              </div>
            </div>
          </div>
        </div>
      </header>

      <div class="slide-one-item home-slider owl-carousel">
        <div
          class="site-blocks-cover overlay"
          style="background-image: url(images/beaches1.jpg)"
          data-aos="fade"
          data-stellar-background-ratio="0.5"
        >
          <div class="container">
            <div
              class="row align-items-center justify-content-center text-center"
            >
              <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                <h1 class="text-white font-weight-light">
                  Never Stop Exploring
                </h1>
                <p class="mb-5">
                  We present you to the finest beaches in the world
                </p>
                <p>
                  <a
                    href="blog.php"
                    class="btn btn-primary py-3 px-5 text-white"
                    >Explore Now!</a
                  >
                </p>
              </div>
            </div>
          </div>
        </div>

        <div
          class="site-blocks-cover overlay"
          style="background-image: url(images/beaches2.jpg)"
          data-aos="fade"
          data-stellar-background-ratio="0.5"
        >
          <div class="container">
            <div
              class="row align-items-center justify-content-center text-center"
            >
              <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                <h1 class="text-white font-weight-light">Love The Places</h1>
                <p class="mb-5">
                  Whichever beaches you go, we are always there to support
                </p>
                <p>
                  <a
                    href="blog.php"
                    class="btn btn-primary py-3 px-5 text-white"
                    >Explore Now!</a
                  >
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="site-section">
        <div class="container overlap-section">
          <div class="row">
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
              <a href="contact.php" class="unit-1 text-center">
                <img src="images/beaches3.jpg" alt="Image" class="img-fluid" />
                <div class="unit-1-text">
                  <h3 class="unit-1-heading">Write Down Your Experience</h3>
                </div>
              </a>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
              <a href="blog.php" class="unit-1 text-center">
                <img src="images/beaches4.jpg" alt="Image" class="img-fluid" />
                <div class="unit-1-text">
                  <h3 class="unit-1-heading">Explore worlds famous beaches</h3>
                </div>
              </a>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
              <a href="booking.php" class="unit-1 text-center">
                <img src="images/beaches5.jpg" alt="Image" class="img-fluid" />
                <div class="unit-1-text">
                  <h3 class="unit-1-heading">Safe Trip With Airasia</h3>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>

      <div class="site-section">
        <div class="container">
          <div class="row align-items-stretch">
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4">
                  <span class="text-primary flaticon-airplane"></span>
                </div>
                <div>
                  <h3>Air Ticketing</h3>
                  <p>
                    We provides the easiest way of travelling that you can
                    easily book.
                  </p>
                  <p><a href="booking.php">Book now</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4">
                  <span class="text-primary flaticon-ship"></span>
                </div>
                <div>
                  <h3>Cruises</h3>
                  <p>We have 24/24 only support for you whenever you need</p>
                  <p><a href="booking.php">Book now</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4">
                  <span class="text-primary flaticon-route"></span>
                </div>
                <div>
                  <h3>Tour Packages</h3>
                  <p>Hundreds of good priced tour awaits you</p>
                  <p><a href="booking.php">Book now</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="site-section block-13 bg-light">
        <div class="container">
          <div class="row justify-content-center mb-5">
            <div class="col-md-7">
              <h2 class="font-weight-light text-black text-center">
                Testimonials
              </h2>
            </div>
          </div>

          <div class="nonloop-block-13 owl-carousel">
            <div class="item">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 mb-5">
                    <img
                      src="images/duong.jpg"
                      alt="Image"
                      class="img-md-fluid"
                    />
                  </div>
                  <div
                    class="overlap-left col-lg-6 bg-white p-md-5 align-self-center"
                  >
                    <p class="text-black lead">
                      &ldquo;Travelling with BeautyBeaches is a good experience
                      that I have had!!!&rdquo;
                    </p>
                    <p class="">
                      &mdash; <em>Hai Duong Nguyen</em>,
                      <a href="#">Traveler</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 mb-5">
                    <img
                      src="images/long.jpg"
                      alt="Image"
                      class="img-md-fluid"
                    />
                  </div>
                  <div
                    class="overlap-left col-lg-6 bg-white p-md-5 align-self-center"
                  >
                    <p class="text-black lead">
                      &ldquo;As a frequent traveller, it is very easy using
                      BeautyBeaches services&rdquo;
                    </p>
                    <p class="">
                      &mdash; <em>Ngoc Long Vu</em>, <a href="#">Journalist</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 mb-5">
                    <img
                      src="images/hoang.jpg"
                      alt="Image"
                      class="img-md-fluid"
                    />
                  </div>
                  <div
                    class="overlap-left col-lg-6 bg-white p-md-5 align-self-center"
                  >
                    <p class="text-black lead">
                      &ldquo;If you have an oppertunity, do not miss on
                      BeautyBeaches&rdquo;
                    </p>
                    <p class="">
                      &mdash; <em>Viet Hoang Tran</em>,
                      <a href="#">Aptech Journalist</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="site-section">
        <div class="container">
          <div class="row justify-content-center mb-5">
            <div class="col-md-7 text-center">
              <h2 class="font-weight-light text-black">Our Destinations</h2>
              <p class="color-black-opacity-5">Choose Your Next Destination</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <a href="destination.php" class="unit-1 text-center">
                <img src="images/halongbay.jpg" alt="Image" class="img-fluid" />
                <div class="unit-1-text">
                  <strong class="cl mb-2 d-block"
                    >Giá Niêm Khiết: &emsp;<strike style="color: white">
                      6.500.000 đ</strike
                    ></strong
                  >
                  <strong class="mb-2 d-block" style="color: red"
                    >Giá Khuyến Mại: &emsp;3.250.000 đ</strong
                  >
                  <h3 class="unit-1-heading">Ha Long,VietNam</h3>
                </div>
              </a>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <a href="destination.php" class="unit-1 text-center">
                <img src="images/phuquoc.jpg" alt="Image" class="img-fluid" />
                <div class="unit-1-text">
                  <strong class="cl mb-2 d-block"
                    >Giá Niêm Khiết: &emsp;<strike style="color: white">
                      5.500.000 đ</strike
                    ></strong
                  >
                  <strong class="mb-2 d-block" style="color: red"
                    >Giá Khuyến Mại: &emsp;2.750.000 đ</strong
                  >
                  <h3 class="unit-1-heading">Phu Quoc,VietNam</h3>
                </div>
              </a>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <a href="destination.php" class="unit-1 text-center">
                <img src="images/vinpearl.jpg" alt="Image" class="img-fluid" />
                <div class="unit-1-text">
                  <strong class="cl mb-2 d-block"
                    >Giá Niêm Khiết: &emsp;<strike style="color: white">
                      6.000.000 đ</strike
                    ></strong
                  >
                  <strong class="mb-2 d-block" style="color: red"
                    >Giá Khuyến Mại: &emsp;3.000.000 đ</strong
                  >
                  <h3 class="unit-1-heading">Nha Trang,Vietnam</h3>
                </div>
              </a>
            </div>

            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <a href="destination.php" class="unit-1 text-center">
                <img src="images/hawaii.jpg" alt="Image" class="img-fluid" />
                <div class="unit-1-text">
                  <strong class="cl mb-2 d-block"
                    >Giá Niêm Khiết: &emsp;<strike style="color: white">
                      7.000.000 đ</strike
                    ></strong
                  >
                  <strong class="mb-2 d-block" style="color: red"
                    >Giá Khuyến Mại: &emsp;3.500.000 đ</strong
                  >
                  <h3 class="unit-1-heading">Hawaii, U.S</h3>
                </div>
              </a>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <a href="destination.php" class="unit-1 text-center">
                <img src="images/maldives.jpg" alt="Image" class="img-fluid" />
                <div class="unit-1-text">
                  <strong class="mb-2 d-block cl"
                    >Giá Niêm Khiết: &emsp;<strike style="color: white">
                      7.500.000 đ</strike
                    ></strong
                  >
                  <strong class="mb-2 d-block" style="color: red"
                    >Giá Khuyến Mại: &emsp;3.750.000 đ</strong
                  >
                  <h3 class="unit-1-heading">Maldives, Indian Ocean</h3>
                </div>
              </a>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <a href="destination.php" class="unit-1 text-center">
                <img src="images/bali.jpg" alt="Image" class="img-fluid" />
                <div class="unit-1-text">
                  <strong class="mb-2 d-block cl"
                    >Giá Niêm Khiết: &emsp;<strike style="color: white">
                      5.500.000 đ</strike
                    ></strong
                  >
                  <strong class="mb-2 d-block" style="color: red"
                    >Giá Khuyến Mại: &emsp;2.750.000 đ</strong
                  >
                  <h3 class="unit-1-heading">Bali, Indonesia</h3>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>

      <!-- <div class="site-section bg-light">
      
    </div> -->

      <div
        class="site-blocks-cover overlay inner-page-cover"
        style="
          background-image: url(images/maldives.jpg);
          background-attachment: fixed;
        "
      >
        <div class="container">
          <div
            class="row align-items-center justify-content-center text-center"
          >
            <div class="col-md-7" data-aos="fade-up" data-aos-delay="400">
              <a
                ><span class="icon-play"
                  ><iframe
                    width="560"
                    height="315"
                    src="https://www.youtube.com/embed/3Ay4Sk7NRCY"
                    title="YouTube video player"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                  ></iframe></span
              ></a>
              <h2 class="text-white font-weight-light mb-5 h1">
                Experience Our Outstanding Services
              </h2>
            </div>
          </div>
        </div>
      </div>

      <div class="site-section bg-light">
        <div class="container">
          <div class="row justify-content-center mb-5">
            <div class="col-md-7 text-center">
              <h2 class="font-weight-light text-black">Our Services</h2>
              <p class="color-black-opacity-5">
                We Offer The Following Services
              </p>
            </div>
          </div>
          <div class="row align-items-stretch">
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4">
                  <span class="text-primary flaticon-airplane"></span>
                </div>
                <div>
                  <h3>Air Ticketing</h3>
                  <p>Various flights awaits you from many airport</p>
                  <p><a href="booking.php">Book now</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4">
                  <span class="text-primary flaticon-ship"></span>
                </div>
                <div>
                  <h3>Cruises</h3>
                  <p>If you want to travel on sea, we will get you ready.</p>
                  <p><a href="booking.php">Book now</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4">
                  <span class="text-primary flaticon-route"></span>
                </div>
                <div>
                  <h3>Tour Packages</h3>
                  <p>Exclusive deal just for only you</p>
                  <p><a href="booking.php">Book now</a></p>
                </div>
              </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4">
                  <span class="text-primary flaticon-hotel"></span>
                </div>
                <div>
                  <h3>Hotel Accomodations</h3>
                  <p>
                    We have connected with hundres of onling booking application
                  </p>
                  <p><a href="booking.php">Book now</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4">
                  <span class="text-primary flaticon-sailboat"></span>
                </div>
                <div>
                  <h3>Sea Explorations</h3>
                  <p>
                    We will connect you to the locals for your finest experience
                  </p>
                  <p><a href="booking.php">Book now</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4">
                  <span class="text-primary flaticon-ski"></span>
                </div>
                <div>
                  <h3>Ski Experiences</h3>
                  <p>Only if you want adventurous feeling you never forget</p>
                  <p><a href="booking.php">Book now</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="site-section">
        <div class="container">
          <div class="row justify-content-center mb-5">
            <div class="col-md-7 text-center">
              <h2 class="font-weight-light text-black">Our Blog</h2>
              <p class="color-black-opacity-5">
                See Our Daily News &amp; Updates
              </p>
            </div>
          </div>
          <div class="row mb-3 align-items-stretch">
            <div class="col-md-6 col-lg-6 mb-4 mb-lg-4">
              <div class="h-entry">
                <img src="images/maldives.jpg" alt="Image" class="img-fluid" />
                <h2 class="font-size-regular">
                  <a href="blog.php">How to Plan Your Vacation</a>
                </h2>
                <div class="meta mb-4">
                  by Viet Hoang Tran <span class="mx-2">&bullet;</span> Jan 18,
                  2022 at 8:00 pm <span class="mx-2">&bullet;</span>
                  <a href="blog.php">News</a>
                </div>
                <p>Get you pack ready and join BeautyBeaches now!!!</p>
              </div>
            </div>
            <div class="col-md-6 col-lg-6 mb-4 mb-lg-4">
              <div class="h-entry">
                <img src="images/halongbay.jpg" alt="Image" class="img-fluid" />
                <h2 class="font-size-regular">
                  <a href="blog.php">How to Plan Your Vacation</a>
                </h2>
                <div class="meta mb-4">
                  by Viet Hoang Tran <span class="mx-2">&bullet;</span> Jan 18,
                  2022 at 8:00 pm <span class="mx-2">&bullet;</span>
                  <a href="blog.php">News</a>
                </div>
                <p>Planning a vacation with BeautyBeaches is super easy!!!</p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 text-center">
              <a
                href="blog.php"
                class="btn btn-outline-primary border-2 py-3 px-5"
                >View All Blog Posts</a
              >
            </div>
          </div>
        </div>
      </div>

      <div class="site-section border-top">
        <div class="container">
          <div class="row text-center">
            <div class="col-md-12">
              <h2 class="mb-5 text-black">Want To Travel With Us?</h2>
              <p class="mb-0">
                <a
                  href="booking.php"
                  class="btn btn-primary py-3 px-5 text-white"
                  >Book Now</a
                >
              </p>
            </div>
          </div>
        </div>
      </div>

      <footer class="site-footer">
        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <div class="mb-5">
                <h3 class="footer-heading mb-4">About BeautyBeaches</h3>
                <p>
                  Nurtured from the seed of a single great idea - to empower the
                  traveller - BeautyBeaches is a pioneer in Vietnam's online
                  travel industry. Founded in the year 2000 by TranVietHoang,
                  BeautyBeaches came to life to empower the Vietnamese traveller
                  with instant bookings and comprehensive choices.
                </p>
              </div>
            </div>
            <div class="col-lg-4 mb-5 mb-lg-0">
              <div class="row mb-5">
                <div class="col-md-12">
                  <h3 class="footer-heading mb-4">Navigations</h3>
                </div>
                <div class="col-md-6 col-lg-6">
                  <ul class="list-unstyled">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="destination.php">Destination</a></li>
                    <li><a href="booking.php">Services</a></li>
                    <li><a href="about.php">About</a></li>
                  </ul>
                </div>
                <div class="col-md-6 col-lg-6">
                  <ul class="list-unstyled">
                    <li><a href="about.php">About Us</a></li>
                    <li><a href="contact.php">Privacy Policy</a></li>
                    <li><a href="contact.php">Contact Us</a></li>
                    <li><a href="discount.php">Discounts</a></li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="mb-5">
              <h3 class="footer-heading mb-2">Map</h3>
              <div>
                  <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.924403452778!2d105.81902919999999!3d21.035710599999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab145bf89bd7%3A0xd94a869b494c04b6!2zMjg1IMSQ4buZaSBD4bqlbiwgVsSpbmggUGjDuiwgQmEgxJDDrG5oLCBIw6AgTuG7mWkgMTAwMDAw!5e0!3m2!1svi!2s!4v1657635191197!5m2!1svi!2s"
                    width="600"
                    height="450"
                    style="border: 0"
                    allowfullscreen=""
                    loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"
                  ></iframe>
                </div>
            </div>
          </div>
          </div>
          <div class="row pt-5 mt-5 text-center">
            <div class="col-md-12">
              <div class="mb-5">
                <a href="http://facebook.com" class="pl-0 pr-3"
                  ><span class="icon-facebook"></span
                ></a>
                <a href="http://twitter.com" class="pl-3 pr-3"
                  ><span class="icon-twitter"></span
                ></a>
                <a href="http://instagram.com" class="pl-3 pr-3"
                  ><span class="icon-instagram"></span
                ></a>
                <a href="http://linkedin.com" class="pl-3 pr-3"
                  ><span class="icon-linkedin"></span
                ></a>
              </div>

              <p>
                Copyright &copy;
                <script>
                  document.write(new Date().getFullYear());
                </script>
                All rights reserved by Group 2
              </p>
            </div>
          </div>
        </div>
      </footer>
    </div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/jquery-migrate-3.0.1.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/aos.js"></script>
    <!--   <script src="js/time.js"></script> -->

    <script src="js/main.js"></script>
  </body>
</html>
