<!DOCTYPE html>
<html lang="en">
  <head>
    <title>BeautyBeaches &mdash;All you can go!</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700"> 
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">


    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>
  <body>
  
  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    
    <header class="site-navbar py-1" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
            <h1 class="mb-0"><a href="index.php" class="text-black h2 mb-0">BeautyBeaches</a></h1>
          </div>
          <div class="col-10 col-md-8 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">

              <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                <li class="active">
                  <a href="index.php">Home</a>
                </li>
                <li  >
                  <a href="destination.php">Location</a>
                  
                </li>
                
                <li><a href="discount.php">Discount</a></li>
                <li><a href="booking.php">Book</a></li>
                <li><a href="about.php">About</a></li>
                <li >
                    <a href="blog.php">Blog</a>
                      <li>
                        <a href="blog.php"></a>
                      </li>
                  </li>
                
                <li><a href="contact.php">Contact</a></li>
                <!-- <li><a href="readings.html">Readings</a></li> -->
              </ul>
            </nav>
          </div>

          <div class="col-6 col-xl-2 text-right">
            <div class="d-none d-xl-inline-block">
              <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
                <li>
                  <a href="https://www.tripadvisor.com.vn/" target="_blank" class="pl-0 pr-3 text-black"><span class="icon-tripadvisor"></span></a>
                </li>
                <li>
                  <a href="https://twitter.com/?lang=vi" target="_blank" class="pl-3 pr-3 text-black"><span class="icon-twitter"></span></a>
                </li>
                <li>
                  <a href="https://www.facebook.com/" target="_blank" class="pl-3 pr-3 text-black"><span class="icon-facebook"></span></a>
                </li>
                <li>
                  <a href="https://www.instagram.com/" target="_blank" class="pl-3 pr-3 text-black"><span class="icon-instagram"></span></a>
                </li>

                
              </ul>
            </div>

            <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="index.php" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

          </div>

        </div>
      </div>
      
    </header>

  

   

    <div class="site-blocks-cover inner-page-cover" style="background-image: url(images/img_4.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
              <h1 class="text-white font-weight-light">Get In Touch</h1>
              <div><a href="index.php">Home</a> <span class="mx-2 text-white">&bullet;</span> <span class="text-white">Contact</span></div>
              
            </div>
          </div>
        </div>
      </div>  


    
    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-7 mb-5">

            

            <form action="#" class="p-5 bg-white" method='post'>
              <?php 
                require_once 'connect.php';
                if(isset($_POST['submit'])){
                  $firstname= htmlspecialchars($_POST['firstName']);
                  $lastname= htmlspecialchars($_POST['lastName']);
                  $email=htmlspecialchars($_POST['email']);
                  $subject=htmlspecialchars($_POST['subject']);
                  $message=htmlspecialchars($_POST['message']);
                  $sql= sprintf("insert into contact values('$firstname','$lastname','$email','$subject','$message')");
                  $conn_createdb->query($sql);
                  echo 'Cảm ơn đã gửi tin nhắn cho chúng tôi !!!';
                }
              ?>
             

              <div class="row form-group">
                <div class="col-md-6 mb-3 mb-md-0">
                  <label class="text-black" for="fname">First Name</label>
                  <input type="text" id="fname" class="form-control" name="firstName">
                </div>
                <div class="col-md-6">
                  <label class="text-black" for="lname">Last Name</label>
                  <input type="text" id="lname" class="form-control" name="lastName">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="email">Email</label> 
                  <input type="email" id="email" class="form-control" name="email">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="subject">Subject</label> 
                  <input type="subject" id="subject" class="form-control" name="subject">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <label class="text-black" for="message">Message</label> 
                  <textarea name="message" id="message" cols="30" rows="7" class="form-control" placeholder="Write your notes or questions here..." name="message"></textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <input name="submit" type="submit" value="Send Message" class="btn btn-primary py-2 px-4 text-white">
                </div>
              </div>

  
            </form>
          </div>
          <div class="col-md-5">
            
            <div class="p-4 mb-3 bg-white">
              <p class="mb-0 font-weight-bold">Address</p>
              <p class="mb-4">285 Đội Cấn Ba Đình Hà Nội Việt Nam</p>

              <p class="mb-0 font-weight-bold">Phone</p>
              <p class="mb-4"><a href="#">1800 1147</a></p>

              <p class="mb-0 font-weight-bold">Email Address</p>
              <p class="mb-0"><a href="http://gmail.com">aptech3@aprotrain.com</a></p>

            </div>
            
            <div class="p-4 mb-3 bg-white">
              <img src="images/hoang.jpg" alt="Image" class="img-fluid mb-4 rounded">
              <h3 class="h5 text-black mb-3">More Info</h3>
              <p>Please send us your message. We will try our best to support and to bring you happy moments!</p>
              <p><a href="blog.php" class="btn btn-primaThe pain itself is a lot of pain The resultant often finds fault with the truth, and, does it follow by reason, and not by the most worthy fault? As for the truth, which one who flatters the whole of it, is the least bit less hated!ry px-4 py-2 text-white">Learn More</a></p>
            </div>

          </div>
        </div>
      </div>
    </div>

    
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="mb-5">
              <h3 class="footer-heading mb-4">About BeautyBeaches</h3>
              <p>Nurtured from the seed of a single great idea - to empower the traveller - BeautyBeaches is a pioneer in Vietnam's online travel industry. Founded in the year 2000 by TranVietHoang, BeautyBeaches came to life to empower the Vietnamese traveller with instant bookings and comprehensive choices.</p>
            </div>

            
            
          </div>
          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="row mb-5">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">Navigations</h3>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><a href="index.php">Home</a></li>
                  <li><a href="destination.php">Destination</a></li>
                  <li><a href="booking.php">Services</a></li>
                  <li><a href="about.php">About</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><a href="about.php">About Us</a></li>
                  <li><a href="contact.php">Privacy Policy</a></li>
                  <li><a href="contact.php">Contact Us</a></li>
                  <li><a href="discount.php">Discounts</a></li>
                </ul>
              </div>
            </div>
              </div>

          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="mb-5">
              <h3 class="footer-heading mb-2">Map</h3>
              <div>
                  <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.924403452778!2d105.81902919999999!3d21.035710599999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab145bf89bd7%3A0xd94a869b494c04b6!2zMjg1IMSQ4buZaSBD4bqlbiwgVsSpbmggUGjDuiwgQmEgxJDDrG5oLCBIw6AgTuG7mWkgMTAwMDAw!5e0!3m2!1svi!2s!4v1657635191197!5m2!1svi!2s"
                    width="600"
                    height="450"
                    style="border: 0"
                    allowfullscreen=""
                    loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"
                  ></iframe>
                </div>
            </div>
          </div>
          
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="mb-5">
              <a href="https://www.facebook.com/" target="_blank" class="pl-3 pr-3 text-black"><span class="icon-facebook"></span></a>
              <a href="https://twitter.com/?lang=vi" target="_blank" class="pl-3 pr-3 text-black"><span class="icon-twitter"></span></a>
             <a href="https://www.instagram.com/" target="_blank" class="pl-3 pr-3 text-black"><span class="icon-instagram"></span></a>
              <a href="https://www.linkedin.com/" target="_blank" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
            </div>

             <p>
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved 
            </p>
          </div>
          
        </div>
      </div>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>
    
  </body>
</html>