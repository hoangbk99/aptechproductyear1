<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>REGISTER FORM</title>
</head>
<?php 
    require_once "connect.php";
    $usn_errors = "";
    $psw_errors = "";
    $validated = true;
    if(isset($_POST['submit'])){
        if($_POST['username'] == '' || empty($_POST['username'])){
            $usn_errors .= "<p>Username required!</p>";
            $validated = false;
         //Kiem tra da dien username chua
        }
        if($_POST['username']=='admin'){
            $usn_errors = "This username is invalid";
            $validated=false;
        }
        $username = htmlspecialchars($_POST['username']);
        $password = htmlspecialchars($_POST['password']);
        $firstName= htmlspecialchars($_POST['firstName']);
        $lastName= htmlspecialchars($_POST['lastName']);
        $email = htmlspecialchars($_POST['email']);
       
        $sql = sprintf("select * from user where username = '%s'", $username);
        $result = $conn_createdb->query($sql);
        if($result->num_rows >0){
            $usn_errors .= "<p>Username existed!</p>";
             $validated = false;
        }  
        if($_POST['password'] == '' || empty($_POST['password'])){
            $psw_errors .= "<p>Password required!</p>";
             $validated = false;
        }
        if($validated == true){
            $sql=sprintf("insert into user(username,password,firstName,lastName,email) values('%s','%s','%s','%s','%s')",$username,$password,$firstName,$lastName,$email);
            $result = $conn_createdb->query($sql);
            if($result===true){
                echo "<p>Register succeed!</p>";
                echo "<p>Username: $username</p>";
                echo "<p>Password: $password</p>";
                echo "<p>Please note down your password!</p>";
                echo "<p>Email: $email</p>";
                echo "<p><a href='userlogin.php'>Login</a></p>";
                return;
            }else{
                echo "<p>Register failed!</p>";
            }
        }
    }
?>
<body  style="background-image: url('images/maldives.jpg'); background-size:cover">
    <form action="" method="post" class="login-form">
    <div class="login-page">
    <link rel="stylesheet" href="login.css">
    <div class="form">
    <div style="padding-bottom: 25px;color:mediumturquoise">REGISTER FORM</div>
        Username: <input type="text" name="username" required/>
        <?php 
            echo $usn_errors;
        ?>
        <br>
        Password: <input type="password" name="password"  required>
         <?php 
            echo $psw_errors;
        ?>
        <br>
        First Name: <input type="text" name="firstName"  required>
        <br>
        <br>
        Last Name: <input type="text" name="lastName"  required>
        <br>
        <br>
        Email: <input type="email" name="email"  required>
        <br>
        
        <input style="color:green;background-color:mediumaquamarine" type="submit" value="Get me an account!!!" name="submit">
    </div>
    </div>
    </form>
</body>
</html>